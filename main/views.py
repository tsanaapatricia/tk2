from django.shortcuts import render, redirect
from .models import Like
from form.models import PostModel
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

def home(request):
    count_post = PostModel.objects.all().count()
    like_post = {}
    like_count = {}
    comment_count = {}
    pos_id = []
    if request.user.is_authenticated :
        for pos in PostModel.objects.all():
            pos_id.append(pos.id)
            like_count[pos.id] = pos.like_set.all().count()
            comment_count[pos.id] = pos.comment_set.all().count()
            if pos.like_set.filter(user_kudos=request.user) and True or False:
                like_post[pos.id] = 1
            else:
                like_post[pos.id] = 0

        postingan1 = {
            'count_post' : count_post,
            'postingan' : PostModel.objects.all(),
            'pos_id' : pos_id,
            'like_count' : like_count,
            'like_post' : like_post,
            'comment_count' : comment_count
        }
        return render(request, 'main/home.html', postingan1)
    messages.warning(request, 'Please login to continue')
    return redirect('/login/')

@login_required
def kudos_or_undo(request):
    if request.method == 'GET':
        post_id = request.GET['post_id']
        new_kudos, like_it = Like.objects.get_or_create(user_kudos=request.user, post_kudos=PostModel.objects.get(id=post_id))
        if not like_it:
            new_kudos.delete()
            return HttpResponse("You're Unlike this Post")
        return HttpResponse("You're Like this Post")
    else:
        return HttpResponse("Action Failed")