from django.db import models
from django.contrib.auth.models import User
from form.models import PostModel

# Create your models here.
class Like(models.Model):
    user_kudos = models.ForeignKey(User, on_delete=models.CASCADE)
    post_kudos = models.ForeignKey(PostModel, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
