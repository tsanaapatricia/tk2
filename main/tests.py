from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import resolve, reverse
from selenium import webdriver
from .views import home, kudos_or_undo
from .models import Like
from form.models import PostModel
from users.models import Profile
from django.contrib.auth.models import User


class TestHome(TestCase):
    def setUp(self):
        self.user = User(username="Syam", password="apayak123")
        self.user.save()

    def test_home_url_not_login(self):
        response = Client().get('')
        self.assertEqual(response.status_code,302)

    def test_home_login(self):
        c = Client()
        login = c.force_login(self.user)
        response = c.get('')

        self.assertEqual(str(response.context['user']), 'Syam')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/home.html')

    def test_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
        
class TestAddKudos(TestCase):
    def setUp(self):
        user1 = User(username="Syam", password="apayak123")
        user1.save()
        profile1 = Profile(user=user1)
        profile1.save()
        pos1 = PostModel(title="Gajah", content="Itu besar", category="Hewan", author=user1)
        pos1.save()
        kudos1 = Like(user_kudos=user1, post_kudos=pos1)
        kudos1.save()

    def test_kudos_url_post_is_exist(self):
        response = Client().get(reverse('main:kudos'), post_id = 1)
        self.assertEqual(response.status_code, 302)

    def test_add_kudos_func(self):
        found = resolve('/kudos/')
        self.assertEqual(found.func, kudos_or_undo)

    def test_kudos_model_create_new_object(self):
        self.assertEqual(Like.objects.all().count(), 1)

    # def test_kudos_or_undo(self):
    #     self.assertEqual(PostModel.objects.get(id=1).like_set.all().count(), 1)

    #     user2 = User(username="Mary", password="apayak456")
    #     user2.save()
    #     liked2 = PostModel.objects.get(id=1).like_set.filter(user_kudos=user2) and True or False
    #     self.assertEqual(liked2, False)

    #     kudos2 = Like(user_kudos=user2, post_kudos=PostModel.objects.get(id=1))
    #     kudos2.save()
    #     liked2 = PostModel.objects.get(id=1).like_set.filter(user_kudos=user2) and True or False
    #     self.assertEqual(liked2, True)
    #     self.assertEqual(PostModel.objects.get(id=1).like_set.all().count(), 2)

    #     new_kudos, like_it = Like.objects.get_or_create(user_kudos=user2, post_kudos=PostModel.objects.get(id=1))
    #     if not like_it:
    #         new_kudos.delete()
    #     self.assertEqual(PostModel.objects.get(id=1).like_set.all().count(), 1)

    # def test_add_post_kudos(self):
    #     user2 = User(username="Mary", password="apayak456")
    #     user2.save()
    #     pos2 = PostModel(title="Kucing", content="Itu lucu", category="Hewan", author=user2)
    #     pos2.save()
    #     self.assertEqual(PostModel.objects.get(id=1).like_set.all().count(), 1)
    #     self.assertEqual(PostModel.objects.get(id=2).like_set.all().count(), 0)