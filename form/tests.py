from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import PostModel
from .views import add, index
from .forms import FormPost
from .apps import FormConfig
from django.contrib.auth.models import User
# Create your tests here.

class ModelsTest(TestCase):  
    def setUp(self):
        self.new_user = User.objects.create_user("test",password='testpasstest123')
        self.new_user.save()
        self.PostModel = PostModel.objects.create(title="Belajar Coding", content="Belajar bahasa pemrograman", category="Belajar", author=self.new_user)

    def test_instance_created(self):
        self.assertEqual(PostModel.objects.count(), 1)

    # def test_str(self):
    #     self.assertEqual(str(self.PostModel), "Belajar Coding")

class FormTest(TestCase):
    def test_form_is_valid(self):
        form = FormPost(data={
            "title":"Belajar Coding",
            "content":"Belajar bahasa pemrograman",
            "category":"Belajar"
        })
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = FormPost(data={})
        self.assertFalse(form.is_valid())

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.new_user = User.objects.create_user("test",password='testpasstest123')
        self.new_user.save()
        self.PostModel = PostModel.objects.create(title="Belajar Coding", content="Belajar bahasa pemrograman", category="Belajar", author=self.new_user)
        self.response = self.client.login(
            username = "test",
            password = "testpasstest123"
        )
        self.PostModel = PostModel.objects.create(
            title="Belajar Coding", content="Belajar bahasa pemrograman", category="Belajar", author=self.new_user)
        self.index = reverse("form:index")
        self.add = reverse("form:add")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_GET_add(self):
        response = self.client.get(self.add)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'add.html')

    def test_POST_add(self):
        response = self.client.post(self.add,{'title': 'Belajar Coding', 'content': "Belajar bahasa pemrograman", 'category': "Belajar"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_add_invalid(self):
        response = self.client.post(self.add,{'title': '', 'content': "", 'category': ""}, follow = True)
        self.assertTemplateUsed(response, 'add.html')
    
class UrlsTest(TestCase):
    def test_add_use_right_function(self):
        found = resolve('/form/add/')
        self.assertEqual(found.func, add)

    def test_index_use_right_function(self):
        found = resolve('/form/index/')
        self.assertEqual(found.func, index)

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(FormConfig.name, 'form')
