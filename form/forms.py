from django import forms

class FormPost(forms.Form):
    title=forms.CharField(
        label = "Title",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    content=forms.CharField(
        label = "Content",
        max_length=100,
        widget=forms.Textarea(
            attrs={
                'class':'form-control',
            }
        )
    )
    category=forms.CharField(
        label = "Category",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )
    
