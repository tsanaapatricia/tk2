from django.shortcuts import render, redirect, reverse
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.models import User
from .models import Profile, User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from form.models import PostModel
from django.http import HttpResponse, JsonResponse
from django.core.serializers import serialize
import json

# Create your views here.
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            new_user = authenticate(
                username = username,
                password = form.cleaned_data['password1']
            )
            new_profile = Profile.objects.create(user=new_user)
            # messages.success(request, f'Account {username} has been created. Please Log In!')
            
            return HttpResponse('success')
    else :
        form = UserRegisterForm()
        return render(request,'user/register.html',{'form': form, 'usercount': User.objects.all().count() })

# def registerCheck(request):
#     form = UserRegisterForm(request.POST)
#     if (form.is_valid()!=True):
#         return HttpResponse('failed')
#     else : 
#         return HttpResponse('success')


def getDataAllUser(request):
    data_raw = serialize('json', User.objects.all())
    data = json.loads(data_raw)
    return JsonResponse(data,safe=False)

@login_required
def profile(request):
    context = {
        'posts' : PostModel.objects.filter(author=request.user),
        'author':request.user
    }
    return render(request, 'user/profile.html',context)

@login_required
def profile_update(request):
    if request.method == 'POST':
        form_1 = UserUpdateForm(request.POST, instance=request.user)
        form_2 = ProfileUpdateForm(request.POST, request.FILES, instance =request.user.profile)

        if form_1.is_valid() and form_2.is_valid() :
            form_1.save()
            form_2.save()

            messages.success(request, "Updated!")
            return redirect('users:profile')

    else:
        form_1 = UserUpdateForm(instance=request.user)
        form_2 = ProfileUpdateForm(instance =request.user.profile)
    context ={
        'form_1' : form_1,
        'form_2' : form_2
    }
    return render(request, 'user/profile_update.html',context)
