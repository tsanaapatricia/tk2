from django.contrib import admin
from .models import ModelFeedback

# Register your models here.
admin.site.register(ModelFeedback)
