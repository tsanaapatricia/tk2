from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import ModelFeedback
from .views import addFeedback
from .forms import FormFeedback
from .apps import FeedbackConfig
# Create your tests here.

class ModelsTest(TestCase):  
    def setUp(self):
        self.ModelFeedback = ModelFeedback.objects.create(nama="tsanaa", feedback="keren", tanggapan="terima kasih")

    def test_instance_created(self):
        self.assertEqual(ModelFeedback.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.ModelFeedback), "keren")

class FormTest(TestCase):
    # def test_form_is_valid(self):
    #     form = FormFeedback(data={
    #         "nama":"tsanaa",
    #         "feedback":"keren",
    #         "tanggapan":"terima kasih"
    #     })
    #     self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = FormFeedback(data={})
        self.assertFalse(form.is_valid())

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.ModelFeedback = ModelFeedback.objects.create(nama="tsanaa", feedback="keren", tanggapan="terima kasih")
        self.addFeedback = reverse("feedback:addFeedback")

    def test_GET_addFeedback(self):
        response = self.client.get(self.addFeedback)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'feedback.html')

    def test_POST_add(self):
        response = self.client.post(self.addFeedback,{'nama': 'tsanaa', 'feedback': "keren", 'tanggapan': "terima kasih"}, follow = True)
        self.assertEqual(response.status_code, 200)

    # def test_POST_add_invalid(self):
    #     response = self.client.post(self.addFeedback,{'nama': '', 'feedback': "", 'tanggapan': ""}, follow = True)
    #     self.assertTemplateUsed(response, 'feedback.html')

    def test_ajax(self):
        response_get = Client().get("/feedback/addFeedback/", {"nama": "tsanaa", "feedback" : "keren"})
        self.assertEqual(response_get.status_code, 200)
    
class UrlsTest(TestCase):
    def test_add_use_right_function(self):
        found = resolve('/feedback/addFeedback/')
        self.assertEqual(found.func, addFeedback)

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(FeedbackConfig.name, 'feedback')