from django.shortcuts import render
from django.http import JsonResponse
from .models import ModelFeedback

# Create your views here.
def addFeedback(request):
    model = ModelFeedback.objects.all()
    response_data = {}

    if request.method == "POST":
        name = request.POST.get('nama')
        feedback = request.POST.get('feedback')
        response_data['nama'] = name
        response_data['feedback'] = feedback

        ModelFeedback.objects.create(
            nama = name,
            feedback = feedback,
        )

        # model_feedback = ModelFeedback(nama=nama, feedback=feedback)
        # model_feedback.save()
       
        return JsonResponse(response_data)
    return render(request, 'feedback.html', {'model':model}) 
