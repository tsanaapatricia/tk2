from django.db import models

# Create your models here.
class ModelFeedback(models.Model):
    nama = models.CharField(max_length=50)
    feedback = models.TextField(max_length=500)
    tanggapan = models.TextField(blank=True)

    def __str__(self):
        return self.feedback
