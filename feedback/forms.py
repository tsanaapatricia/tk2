from django import forms
from .models import ModelFeedback
class FormFeedback(forms.Form):
    name = forms.CharField(
        label = "Nama",
        max_length = 50,
        widget = forms.TextInput(
            attrs={
            'class' : 'form-control',
            'placeholder' : 'Ngapaini',
            'type' : 'text',
            'required' : True,
            'style': 'margin-bottom: 10px;'
            }
        )
    )
    feedback = forms.CharField(
        label = "Feedback",
        max_length = 500,
        widget = forms.Textarea(
            attrs={
            'class' : 'form-control',
            'placeholder' : 'Website ini sudah sangat bagus',
            'type' : 'text',
            'required' : True,
            'style': 'margin-bottom: 10px;'
            }
        )
    )
