from django.db import models
from django.conf import settings
from form.models import PostModel
from users.models import Profile
# Create your models here.
class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE , default=1)
    komen = models.CharField(max_length=200, blank=False, null=True)
    postingan = models.ForeignKey(PostModel, null=True, on_delete= models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)

    def _str_(self,):
        return self.komen
