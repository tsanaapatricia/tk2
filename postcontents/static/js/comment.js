$(document).ready(function() {
    fetchData();

    $("#id_komen").focus();

    var draftComment = "";
    var signed = $('#logged').val();


    $('#comment-button').click(function() {
        if (signed === 'true') {
            sendComment(draftComment);
        } else {
            alert("Please Login!");
        }
    });


    $("#id_komen").change(function(event) {
        draftComment = event.target.value;
    });

    $('#id_komen').keypress(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if (signed === 'true') {
                sendComment(draftComment);
            } else {
                alert("Please Login!");
            }
        }
    });

});

function fetchData() {
    $.ajax({
        url: "/postcontents/content/1",
        contentType: "application/json",
        dataType: 'json',
        success: function(result) {
            renderComment(result);
        }
    });
}


function renderComment(data) {
    $("#comment-wrapper").empty()

    $.each(data.data, function(i, l) {
        $("#comment-wrapper").append(
            `<div class="row justify-content-center">
            <div class="container comment-container" style="margin-top: 0.1%;">
                <div class="row">
                    <div class="col-12" style="padding-top: 0.7%;    padding-left: 5%;">
                        <h6 id="comment-user-name">` + l.user + `</h6>
                        <h6 id="commentdate">` + l.time + `</h6>
                        <p id="comment">` + l.komen + `</p>
                    </div>
                </div>
            </div>
        </div>`
        );
    });

}



function sendComment(komen) {
    $.post("/postcontents/content/1/", { komen: komen, csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value }, function(response) {
        alert("Comment posted!");
        fetchData();
    });
}