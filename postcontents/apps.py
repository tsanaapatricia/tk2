from django.apps import AppConfig


class PostcontentsConfig(AppConfig):
    name = 'postcontents'
