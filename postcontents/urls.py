from django.urls import path

from . import views
app_name = 'postcontents'

urlpatterns =[
    path('<int:id>/', views.postpage, name='postpage'),
    path('content/<int:id>/', views.contents, name='contents'),
    path('otherprofile/<str:parameter>/', views.otherprofile, name='otherprofile')
] 