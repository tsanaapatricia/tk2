from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.http import JsonResponse
from django.forms.models import model_to_dict
from django.contrib import messages
from form.models import PostModel
from users.models import Profile
from .forms import CommentForm
from django.core import serializers
from .models import Comment
import json

# Create your views here.

def postpage(request, id):
    postobj = PostModel.objects.get(id=id)
    form = CommentForm(request.POST or None)

    context = {
        'postcontent': postobj,
        'comments': [],
        'commentform': form,
    }
    return render(request, 'content.html', context)

def contents(request, id):
    postobj = PostModel.objects.get(id=id)
    form = CommentForm(request.POST or None)
    
    response_data = {}

    if form.is_valid():
        instance = form.save(commit=False)
        instance.postingan = postobj
        instance.user = request.user
        instance.save()

    comments = Comment.objects.all()
    
    final_data = []

    for comment in comments:
        if comment.postingan.id == id:
            draft = {}
            draft['user'] = comment.user.username
            draft['time'] = comment.time
            draft['komen'] = comment.komen
            final_data.append(draft)

    return JsonResponse({'data': final_data, 'status_code': 200})



def otherprofile(request, parameter):
    author = User.objects.get(username=parameter)   
    context = {
        'posts' : PostModel.objects.filter(author=author),
        'author': author
    }
    return render(request, 'user/profile.html', context)
